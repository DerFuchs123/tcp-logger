use std::{
    fs::{File, Permissions},
    io::Write,
    os::unix::fs::PermissionsExt,
    path::Path,
    process::{Command, Stdio},
};

use colored::Colorize;

use crate::PERMISSION_RWX_RW_RW;

pub fn write_tcp_probe_enable(content: impl Into<String>) {
    let content = content.into();
    let mut file = File::create("/sys/kernel/debug/tracing/events/tcp/tcp_probe/enable").unwrap();
    file.write_all(content.as_bytes()).unwrap();
    println!("Set enable to {}", content);
}

pub fn check_program_exists(program: &str) {
    if std::process::Command::new(program)
        .arg("--version")
        .output()
        .is_err()
    {
        panic!("{} {}", program, "is probably not installed!".red())
    }
}

pub fn check_local_program_exists(program: &str) {
    if !Path::new(program).exists() {
        panic!("{} is missing!", program)
    }
}

pub fn start_program_generic(path: &str, stdout_path: &str, args: &str) -> std::process::Child {
    let args: Vec<&str> = args.split(' ').filter(|line| !line.is_empty()).collect();

    println!("Starting '{}'...", path);

    let file = File::create(stdout_path).unwrap();
    let stdout = Stdio::from(file);
    Command::new("sudo")
        .arg(path)
        .stdout(stdout)
        .args(args)
        .spawn()
        .unwrap()
}

/// Starts iperf3 with the provided arguments and **doesn't wait** for it
pub fn start_iperf3(iperf_arguments: Vec<&str>, out_dir: &str) -> std::process::Child {
    println!("Starting iperf3...");
    let file = File::create(format!("{out_dir}/iperf3.log")).unwrap();
    file.set_permissions(Permissions::from_mode(PERMISSION_RWX_RW_RW))
        .unwrap();
    let stdout = Stdio::from(file);
    let one_off_arg = if iperf_arguments.contains(&"-s") {
        "-1"
    } else {
        ""
    };
    Command::new("iperf3")
        .args(iperf_arguments)
        .arg(one_off_arg)
        .stdout(stdout)
        .spawn()
        .unwrap()
}

pub fn start_tcpdump(pcap_name: &str, tcpdump_arguments: Vec<&str>) -> std::process::Child {
    println!("Starting tcpdump...");
    Command::new("tcpdump")
        .args(tcpdump_arguments)
        .arg("-w")
        .arg(pcap_name)
        .stdout(Stdio::null())
        .spawn()
        .unwrap()
}

pub fn send_signal(signal: u8, target_pid: u32) {
    let out = Command::new("sudo")
        .arg("kill")
        .arg("-s")
        .arg(&signal.to_string())
        .arg(&target_pid.to_string())
        .output()
        .unwrap();

    // println!("Kill stdout: {:?}", String::from_utf8(out.stdout));
    // println!("Kill stderr: {:?}", String::from_utf8(out.stderr));
    // println!("Kill exit status: {:?}", out.status);
}
