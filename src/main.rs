use std::{
    fs::File,
    io::{BufRead, BufReader},
    sync::{atomic::AtomicBool, Arc},
    time::{Duration, UNIX_EPOCH},
};

use clap::Parser;
use colored::Colorize;
use types_client::ClientMeasurement;

use crate::{
    cli_args::Cli,
    conversions::{client_pcap_to_csv, server_pcap_to_csv},
    types_server::{ServerDataPoint, ServerMeasurement},
};

pub mod cli_args;
pub mod conversions;
pub mod external;
pub mod io;
pub mod parser;
pub mod plotter;
pub mod types_client;
pub mod types_server;

const PRINT_INTERVAL: Duration = Duration::from_millis(500);
const PERMISSION_RWX_RW_RW: u32 = 0o766;
const SIGINT: u8 = 2;

/*

tcpdump:
    -i <wlp> -> interface
    -s96 -> snaplen of 96 bytes (cut off the rest)
    -U -> unbuffered ??? why ??? If the -w option is specified, make the saved raw  packet  output  ``packet-buffered'';
              i.e., as each packet is saved, it will be written to the output file, rather than being
              written only when the output buffer fills.
    -w <file> -> write to file

*/

// const NFQUEUE_PATH: &str = "../nfqueue-test/target/release/nfqueue-test";
const NFHOOK_PATH: &str = "../nf-hook-module/nf_hook.ko";
const IN_NFHOOK_PATH: &str = "../nf-hook-module/in_nf_hook.ko";
const SERIAL_READER_PATH: &str = "../serial-reader/target/release/serial-reader";

fn main() {
    std::panic::set_hook(Box::new(|info| {
        eprintln!(
            "\n\n{}\n",
            "-------------------- PANIC --------------------".red()
        );

        eprintln!(
            "Panic occurred in {}",
            std::env::args().next().unwrap_or(String::from("unknown"))
        );
        eprintln!("Info (DBG): {:?}\n", info);
        eprintln!("Info: {}\n", info);
        eprintln!("DONT FORGET TO MANUALLY KILL / UNREGISTER");
        eprintln!("\t * tcpdump");
        eprintln!("\t * iperf3");
        eprintln!("\t * serial-reader");
        eprintln!("\t * nf-hook");
        eprintln!(
            "\n{}\n",
            "------------------ PANIC END -------------------".red()
        );
    }));

    let mut cli = Cli::parse();
    println!("{}", cli);

    if !cli.tcpdump_args.is_empty() && cli.pcap_name.is_empty() {
        eprintln!(
            "{}",
            "Argument 'tcpdump_args' set, but 'pcap_name' not".red()
        );
        std::process::exit(1);
    }

    if cli.in_nf_hook && cli.nf_hook {
        eprintln!(
            "{}",
            "Both nf-hook and in-nf-hook are set. Only one can be set".red()
        );
        std::process::exit(1);
    }

    println!("Checking installed Programs...");
    external::check_program_exists("iperf3");
    external::check_program_exists("tshark");
    external::check_program_exists("tcpdump");

    // Create output dir
    std::fs::create_dir_all(&cli.output_dir).unwrap();

    let prepend_output_dir = |dir: &String, elem: String| {
        if elem.is_empty() {
            elem
        } else {
            format!("{dir}/{elem}")
        }
    };

    cli.save_file = prepend_output_dir(&cli.output_dir, cli.save_file);
    cli.csv_name = prepend_output_dir(&cli.output_dir, cli.csv_name);
    cli.pcap_name = prepend_output_dir(&cli.output_dir, cli.pcap_name);
    cli.plots.cwnd_plot = prepend_output_dir(&cli.output_dir, cli.plots.cwnd_plot);
    cli.plots.srtt_plot = prepend_output_dir(&cli.output_dir, cli.plots.srtt_plot);
    cli.plots.throughput_plot = prepend_output_dir(&cli.output_dir, cli.plots.throughput_plot);
    cli.plots.packet_len_plot = prepend_output_dir(&cli.output_dir, cli.plots.packet_len_plot);

    // Only create plots if load file was provided
    if !cli.load_log.is_empty() {
        let mut measurement = ServerMeasurement::from_file(&cli.load_log);
        if !cli.load_csv_s.is_empty() {
            measurement.load_csv_data(&cli.load_csv_s);
        }
        println!("Creating Server Plots...");
        plotter::create_server_plots(&cli, &measurement);

        println!("Exiting...");
        std::process::exit(0);
    }

    // Only create plots if load file was provided
    if !cli.load_csv_c.is_empty() {
        let measurement = ClientMeasurement::from_file(&cli.load_csv_c);
        println!("Creating Client Plots...");
        plotter::create_client_plots(&cli, &measurement);

        println!("Exiting...");
        std::process::exit(0);
    }

    if !cli.convert_pcap_to_csv.is_empty() {
        println!("Converting pcap to csv...");

        let mut split = cli.convert_pcap_to_csv.split(' ');
        let (mode, inp, out) = (
            split.next().unwrap(),
            split.next().unwrap(),
            split.next().unwrap(),
        );
        match mode {
            "s" => server_pcap_to_csv(inp, out, split.next().unwrap()),
            "c" => client_pcap_to_csv(inp, out),
            _ => {
                eprintln!("{}{}", "Invalid mode: ".red(), mode);
                std::process::exit(1);
            }
        }
        std::process::exit(0);
    }

    let running = Arc::new(AtomicBool::new(true));
    let running2 = running.clone();

    ctrlc::set_handler(move || {
        running2.store(false, std::sync::atomic::Ordering::Relaxed);
        println!("{}", "Need one more packet to exit...".yellow());
    })
    .unwrap();

    // Start the bw/serial-reader if flag is set
    let mut serial_reader = None;
    if cli.bandwidth_reader {
        external::check_local_program_exists(SERIAL_READER_PATH);
        serial_reader = Some(external::start_program_generic(
            SERIAL_READER_PATH,
            &format!("{}/serial_reader.log", cli.output_dir),
            &cli.serial_reader_args,
        ));
    }

    if cli.nf_hook {
        println!("Inserting module {}...", NFHOOK_PATH);
        // Wait a bit so that the serial-reader server can start
        std::thread::sleep(Duration::from_millis(500));
        external::check_local_program_exists(NFHOOK_PATH);
        let output = std::process::Command::new("sudo")
            .arg("insmod")
            .arg(NFHOOK_PATH)
            .output()
            .unwrap();
        if output.status.code().unwrap_or(1) != 0 {
            eprintln!(
                "{}{}",
                "Error inserting module: ".red(),
                String::from_utf8_lossy(&output.stderr).magenta()
            );
            std::process::exit(1);
        }
    }

    if cli.in_nf_hook {
        println!("Inserting module {}...", IN_NFHOOK_PATH);
        // Wait a bit so that the serial-reader server can start
        std::thread::sleep(Duration::from_millis(500));
        external::check_local_program_exists(IN_NFHOOK_PATH);
        let output = std::process::Command::new("sudo")
            .arg("insmod")
            .arg(IN_NFHOOK_PATH)
            .output()
            .unwrap();
        if output.status.code().unwrap_or(1) != 0 {
            eprintln!(
                "{}{}",
                "Error inserting module: ".red(),
                String::from_utf8_lossy(&output.stderr).magenta()
            );
            std::process::exit(1);
        }
    }

    // Start the nfqueue-program if arguments were provided
    // let mut nfqueue = None;
    // if !cli.queue_nf.is_empty() {
    //     external::check_local_program_exists(NFQUEUE_PATH);
    //     nfqueue = Some(external::start_program_generic(
    //         NFQUEUE_PATH,
    //         &format!("{}/nfqueue.log", cli.output_dir),
    //         &cli.queue_nf,
    //     ));
    // }

    // Start tcpdump if arguments were provided
    let mut tcp_dump = None;
    if !cli.tcpdump_args.is_empty() {
        tcp_dump = Some(external::start_tcpdump(
            &cli.pcap_name,
            cli.tcpdump_args.split(' ').collect(),
        ));
    }

    external::write_tcp_probe_enable("1");

    // Start iperf3 if arguments were provided
    let mut iperf3 = None;
    if !cli.iperf3_args.is_empty() {
        iperf3 = Some(external::start_iperf3(
            cli.iperf3_args.split(' ').collect(),
            &cli.output_dir,
        ));
    }

    let mut measurement = ServerMeasurement::default();

    let pipe = File::open("/sys/kernel/debug/tracing/trace_pipe").unwrap();
    let mut reader = BufReader::new(pipe);

    let mut ctr = 0;
    let mut ctr_diff = 0;

    let mut timer = std::time::Instant::now();

    println!("Starting Measurement...");
    while running.load(std::sync::atomic::Ordering::Relaxed) {
        // check if iperf exited. If so, also exit
        if let Some(iperf3) = iperf3.as_mut() {
            if iperf3.try_wait().unwrap_or(None).is_some() {
                println!("Iperf3 exited. Exiting...");
                running.store(false, std::sync::atomic::Ordering::Relaxed);
            }
        }

        let mut buf = String::new();
        // pipe.read_to_string(&mut buf).unwrap();
        reader.read_line(&mut buf).unwrap();

        if !running.load(std::sync::atomic::Ordering::Relaxed) {
            break;
        }

        let data = parser::parse_line(&buf);

        if let Some(data) = data {
            // Check skipping condition
            let src_delimiter =
                data["src"].len() - data["src"].chars().rev().position(|c| c == ':').unwrap() - 1;
            let dest_delimiter =
                data["dest"].len() - data["dest"].chars().rev().position(|c| c == ':').unwrap() - 1;

            let is_unequal =
                |cli_arg: &str, comparable: &str| !cli_arg.is_empty() && comparable != cli_arg;

            let src_ip = &data["src"][..src_delimiter];
            let src_port = &data["src"][src_delimiter + 1..];
            let dest_ip = &data["dest"][..dest_delimiter];
            let dest_port = &data["dest"][dest_delimiter + 1..];

            if is_unequal(&cli.address.src_ip, src_ip)
                || is_unequal(&cli.address.dest_ip, dest_ip)
                || is_unequal(&cli.address.src_port, src_port)
                || is_unequal(&cli.address.dest_port, dest_port)
            {
                continue;
            }

            // Save data
            let mut data = ServerDataPoint::from_parsed(&data);
            data.time = std::time::SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .unwrap();
            measurement.add_data(data);
        }

        let current_time = std::time::Instant::now();
        if current_time - timer > PRINT_INTERVAL {
            println!(
                "(#{ctr}): #diff: {ctr_diff}; time(s): {:?} ({:?}); srtt(ms): {}; snd_cwnd(#packs): {}",
                measurement.data.last().unwrap().time - measurement.start_time(),
                measurement.data.last().unwrap().time,
                measurement.data.last().unwrap().srtt,
                measurement.data.last().unwrap().cwnd,
            );
            timer = current_time;
            ctr_diff = 0;
        }

        ctr += 1;
        ctr_diff += 1;
    }

    println!("Finishing up...");

    println!("Disabling tcp_probe...");
    external::write_tcp_probe_enable("0");

    println!("Trimming & Sorting data...");
    // measurement.trim_time();
    measurement.sort();

    println!("Measurements: {}", measurement.summary());

    println!("Saving data...");
    measurement.save_data(&cli.save_file);

    // if let Some(mut nfqueue) = nfqueue {
    //     println!("Killing nfqueue...");
    //     nfqueue.kill().unwrap();
    //     nfqueue.wait().unwrap();
    // }

    println!("Kill all children");
    std::process::Command::new("sudo")
        .arg("pkill")
        .arg("-P")
        .arg(std::process::id().to_string())
        .spawn()
        .unwrap()
        .wait()
        .unwrap();

    if let Some(mut serial_reader) = serial_reader {
        println!("Killing serial-reader...");
        // I dont even know anymore why this doesnt work... So just spam it
        while serial_reader.try_wait().unwrap_or(None).is_none() {
            external::send_signal(SIGINT, serial_reader.id());
        }
        serial_reader.wait().unwrap();
    }

    if let Some(mut iperf3) = iperf3 {
        println!("Killing iperf3...");
        iperf3.kill().unwrap();
        iperf3.wait().unwrap();
    }

    if let Some(mut tcp_dump) = tcp_dump {
        println!("Killing tcp_dump...");
        external::send_signal(SIGINT, tcp_dump.id());
        tcp_dump.wait().unwrap();

        if !cli.csv_name.is_empty() {
            println!("Converting pcap file to csv...");
            client_pcap_to_csv(&cli.pcap_name, &cli.csv_name);
        }
    }

    if cli.nf_hook {
        println!("Unloading nf_hook...");
        std::process::Command::new("sudo")
            .arg("rmmod")
            .arg("nf_hook")
            .output()
            .unwrap();
    }

    if cli.in_nf_hook {
        println!("Unloading in_nf_hook...");
        std::process::Command::new("sudo")
            .arg("rmmod")
            .arg("in_nf_hook")
            .output()
            .unwrap();
    }

    println!("Creating Plots...");
    plotter::create_server_plots(&cli, &measurement);

    if !cli.csv_name.is_empty() {
        let client_measurement = ClientMeasurement::from_file(&cli.csv_name);
        plotter::create_client_plots(&cli, &client_measurement);
    }

    if std::fs::read_dir(&cli.output_dir).unwrap().count() == 0 {
        std::fs::remove_dir(&cli.output_dir).unwrap();
    }

    println!("Done!");
}
