use std::{
    collections::HashMap,
    fmt::Display,
    fs::{File, Permissions},
    io::{Read, Write},
    os::unix::fs::PermissionsExt,
    time::Duration,
};

/// All of these are recorded from the tcp_probe tracepoint
pub struct ServerDataPoint {
    pub time: Duration,
    pub cwnd: u64,
    pub srtt: u64,
    pub snd_wnd: u64,
    pub ssthresh: u64,
}

impl ServerDataPoint {
    pub fn new(
        time: Duration,
        cwnd: u64,
        srtt: u64,
        snd_wnd: u64,
        rcv_wnd: u64,
        ssthresh: u64,
    ) -> Self {
        Self {
            time,
            cwnd,
            srtt,
            snd_wnd,
            ssthresh,
        }
    }

    /// Parses a line which has the same format as specified in *Self::save_format*
    pub fn from_line(line: &str) -> Self {
        let mut split = line.split('|');
        Self {
            time: Duration::from_millis(split.next().unwrap().parse().unwrap()),
            cwnd: split.next().unwrap().parse().unwrap(),
            srtt: split.next().unwrap().parse().unwrap(),

            // Here a default value is provided to keep compatibility with old measurement dat
            snd_wnd: split.next().unwrap_or("0").parse().unwrap(),
            ssthresh: split.next().unwrap_or("0").parse().unwrap(),
        }
    }

    /// Provided the data of this DataPoint in a format that can be saved to a file
    pub fn save_format(&self) -> String {
        format!(
            "{}|{}|{}|{}|{}",
            self.time.as_millis(),
            self.cwnd,
            self.srtt,
            self.snd_wnd,
            self.ssthresh
        )
    }

    pub fn from_parsed(data: &HashMap<String, String>) -> Self {
        let time = data["time"].parse::<f64>().unwrap();

        let time = Duration::from_secs_f64(time);
        Self {
            time,
            cwnd: data["snd_cwnd"].parse().unwrap(),
            srtt: data["srtt"].parse().unwrap(),
            snd_wnd: data["snd_wnd"].parse().unwrap(),
            ssthresh: data["ssthresh"].parse().unwrap(),
        }
    }
}

impl Display for ServerDataPoint {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "time: {:?} \t srtt: {} \t cwnd: {}",
            self.time, self.srtt, self.cwnd
        )
    }
}

#[derive(Default)]
pub struct ServerMeasurement {
    pub data: Vec<ServerDataPoint>,

    /// Vec<(stream_id, mss, Vec<(time, recv_window)>)>
    /// The time is relative to the start of the measurement
    /// The recv_window is in bytes
    pub recv_window: Vec<(usize, u32, Vec<(Duration, u32)>)>,

    /// Vec<stream_id, mss, Vec<(time, bytes_in_flight)>>
    pub bytes_in_flight: Vec<(usize, u32, Vec<(Duration, u32)>)>,
}

impl ServerMeasurement {
    /// Loads measurement data from a log-file with the provided name
    /// * `filename` - The name of the log-file to load the data from <br>
    pub fn from_file(filename: impl Into<String>) -> Self {
        let filename = filename.into();

        let mut file = File::open(filename).unwrap();

        let mut buf = String::new();
        file.read_to_string(&mut buf).unwrap();

        Self {
            data: buf
                .split('\n')
                .filter_map(|line| {
                    if line.is_empty() {
                        None
                    } else {
                        Some(ServerDataPoint::from_line(line))
                    }
                })
                .collect(),
            recv_window: vec![],
            bytes_in_flight: vec![],
        }
    }

    /// Loads the recv_window data from a csv-file
    /// * `filename` - The name of the csv-file to load the data from <br>
    pub fn load_csv_data(&mut self, filename: impl Into<String>) {
        let both_filenames = filename.into();
        let mut split = both_filenames.split(' ');

        let d_filename = split.next().unwrap();
        let s_filename = split.next().unwrap();

        let mut file = File::open(d_filename).unwrap();

        let mut buf = String::new();
        file.read_to_string(&mut buf).unwrap();

        let mut recv_window_data = HashMap::new();

        let mut mss = None;
        let mut start_time = None;

        buf.split('\n')
            .filter(|line| !line.is_empty())
            .for_each(|line| {
                let mut split = line.split('|');
                let stream: usize = split.next().unwrap().parse().unwrap();
                let mut time: f64 = split.next().unwrap().parse().unwrap();
                if start_time.is_none() {
                    start_time = Some(time);
                }
                time -= start_time.unwrap();
                let recv_window: u32 = split.next().unwrap().parse().unwrap();
                if let Some(local_mss) = split.next().and_then(|elem| elem.parse::<u32>().ok()) {
                    mss = Some(local_mss);
                }

                recv_window_data
                    .entry(stream)
                    .or_insert(vec![])
                    .push((Duration::from_secs_f64(time), recv_window));
            });

        self.recv_window = recv_window_data
            .iter()
            .map(|(k, v)| (*k, mss.unwrap_or(1), v.clone()))
            .collect();

        let mut bytes_in_flight_data = HashMap::new();
        let mut start_time = None;
        let mut mss = None;

        let s_content = std::fs::read_to_string(s_filename).unwrap();
        s_content
            .split('\n')
            .filter(|line| !line.is_empty())
            .for_each(|line| {
                let mut split = line.split('|');
                let stream: usize = split.next().unwrap().parse().unwrap();
                let mut time: f64 = split.next().unwrap().parse().unwrap();
                if start_time.is_none() {
                    start_time = Some(time);
                }
                time -= start_time.unwrap();
                let bytes_in_flight: u32 = split.next().unwrap_or("0").parse().unwrap_or(0);
                if let Some(local_mss) = split.next().and_then(|elem| elem.parse::<u32>().ok()) {
                    mss = Some(local_mss);
                }

                bytes_in_flight_data
                    .entry(stream)
                    .or_insert(vec![])
                    .push((Duration::from_secs_f64(time), bytes_in_flight));
            });

        self.bytes_in_flight = bytes_in_flight_data
            .iter()
            .map(|(k, v)| (*k, mss.unwrap_or(1), v.clone()))
            .collect();
    }

    /// Save the measurement data to a file with the provided filename <br>
    /// Load via *Self::from_file*
    pub fn save_data(&self, filename: impl Into<String>) {
        let filename = filename.into();

        let mut file = File::create(filename).unwrap();
        file.set_permissions(Permissions::from_mode(crate::PERMISSION_RWX_RW_RW))
            .unwrap();

        for point in &self.data {
            writeln!(file, "{}", point.save_format()).unwrap();
        }
    }

    /// Sorts the measurements based on the timestamp
    pub fn sort(&mut self) {
        self.data.sort_by_key(|d| d.time);
    }

    /// Adjusts the timestamps so that the measurement starts at 0.0
    /// e.g. subtract the start-time from every entry
    pub fn trim_time(&mut self) {
        let start_time = self.start_time();
        for point in &mut self.data {
            point.time -= start_time;
        }
    }

    /// Provides the start-time of the measurement <br>
    /// e.g. The time of the first Data Point
    pub fn start_time(&self) -> Duration {
        self.data.first().unwrap().time
    }

    /// Adds a DataPoint to this measurement
    pub fn add_data(&mut self, data: ServerDataPoint) {
        self.data.push(data);
    }

    /// Time from the first request until the last request before ctrl+c was pressed
    pub fn total_time(&self) -> Duration {
        self.data.last().unwrap().time - self.start_time()
    }

    /// Provides a short summary of this measurement
    pub fn summary(&self) -> String {
        format!(
            "Measurement took {}.{} seconds",
            self.total_time().as_secs(),
            self.total_time().subsec_millis()
        )
    }
}

impl Display for ServerMeasurement {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(
            f,
            "Start Time: {:?}, Total Time: {:?}",
            self.start_time(),
            self.total_time()
        )
        .unwrap();

        for (ctr, point) in self.data.iter().enumerate() {
            writeln!(f, "\t({ctr}) {}", point).unwrap();
        }

        Ok(())
    }
}
