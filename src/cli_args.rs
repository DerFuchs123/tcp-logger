use clap::Parser;
use std::fmt::Display;

#[derive(Parser, Debug, Clone)]
#[command(version, styles=add_colors())]
pub struct Cli {
    /// The Resolution of the moving average calculation [ms]
    #[clap(long, action, default_value_t = 50)]
    pub ma_resolution: u64,

    /// The Size of the Moving Average Window [ms]
    #[clap(long, action, default_value_t = 750)]
    pub ma_window: u64,

    /// Address-specific filter options
    #[command(flatten)]
    pub address: Addresses,

    /// Plot-specific options
    #[command(flatten)]
    pub plots: Plots,

    /// If set, creates server-side-plots (e.g. cwnd, srtt) based on the given log-file and then quits
    #[clap(long, default_value = "")]
    pub load_log: String,

    /// If set, loads additional data (e.g. recv-window) for the server-side-plots from the provided csv-files <br>
    /// The first csv-file has 5201 as dst-port and the second one has 5201 as src-port <br>
    /// Enter both names seperated by a space <br>
    /// E.g. 'pcap_as_d.csv pcap_as_s.csv' <br>
    #[clap(long, default_value = "")]
    pub load_csv_s: String,

    /// If set, creates client-side-plots (e.g. throughput, pack-len) based on the given csv-file and then quits
    #[clap(long, default_value = "")]
    pub load_csv_c: String,

    /// If set, creates plots based on the given file and then quits
    #[clap(long, default_value = "save_data.log")]
    pub save_file: String,

    /// If set, starts the bw/serial-reader
    #[clap(long, short, action)]
    pub bandwidth_reader: bool,

    /// If the bandwidth-reader is started, this will be the arguments passed to the serial-reader
    #[clap(long, action, default_value = "")]
    pub serial_reader_args: String,

    /// If set, starts the nfqueue forwarder with he provided arguments
    // #[clap(long, short, action, default_value = "")]
    // pub queue_nf: String,

    /// If set, inserts the nf-hook module into the kernel
    #[clap(long, short = 'h', action)]
    pub nf_hook: bool,

    /// If set, inserts the in-nf-hook module into the kernel
    #[clap(long, action)]
    pub in_nf_hook: bool,

    /// If set, starts iperf with the provided arguments
    #[clap(long, short, action, default_value_t = String::new())]
    pub iperf3_args: String,

    /// If set, start tcpdump with the provided arguments <br>
    /// **If this is set the argument 'pcap_name' also has to be set!**
    #[clap(long, short='d', action, default_value_t = String::new())]
    pub tcpdump_args: String,

    /// Extra filters that should be applied to tshark while converting pcap to csv <br>
    /// **No Spaces in the filter string!**
    // #[clap(long, short = 'f', action, default_value = "-Y ip.src==193.31.24.188")]
    // pub tshark_filter: String,

    /// If tcpdump-args is set, this will be the name of the pcap file
    #[clap(long, short, action, default_value_t = String::new())]
    pub pcap_name: String,

    /// If set, convert generated pcap file to csv
    #[clap(long, short='n', action, default_value_t = String::new())]
    pub csv_name: String,

    /// If set, saves the specified files to the provided output dir
    /// Has no effect on the manual pcap to csv conversion (e.g. 'convert_pcap_to_csv')
    #[clap(long, short, action, default_value = "./mes")]
    pub output_dir: String,

    /// If set, the svg-data will be cut off after the given time [s]
    #[clap(long, action, default_value_t = 200)]
    pub svg_data_cutoff_time: u64,

    /// If set, converts the given pcap-file to csv using tshark <br>
    /// Enter the mode ((s)erver or (c)lient) and both filenames. <br>
    /// Everything must be seperated by a space (' ') (e.g. '<s/c> <PCAP_NAME> <CSV_NAME> <SECOND_CSV_NAME(SERVER ONLY)>') <br>
    /// E.g for Server Mode: 's tcpdump_server.pcap pcap_as_d.csv pcap_as_s.csv' <br>
    /// E.g for Client Mode: 's tcpdump_server.pcap pcap_as.csv' <br>
    /// (Remember the -f / --tshark-filter flag for this one if you want to pass extra arguments) <br>
    #[clap(long, action, default_value = "")]
    pub convert_pcap_to_csv: String,
}

#[derive(Parser, Clone, Debug)]
pub struct Addresses {
    /// The src ip that has to match in order to include this packet in the measurement
    #[clap(long, action, default_value = "")]
    pub src_ip: String,

    /// The src port that has to match in order to include this packet in the measurement
    #[clap(long, action, default_value = "")]
    pub src_port: String,

    /// The dest ip that has to match in order to include this packet in the measurement
    #[clap(long, action, default_value = "")]
    pub dest_ip: String,

    /// The dest port that has to match in order to include this packet in the measurement
    #[clap(long, action, default_value = "")]
    pub dest_port: String,
}

#[derive(Parser, Clone, Debug)]
pub struct Plots {
    /// The name of the throughput-plot-file (Leave empty to not create a plot)
    #[clap(long, short, default_value = "")]
    pub throughput_plot: String,

    /// The name of the packet-len-plot-file (Leave empty to not create a plot)
    #[clap(long, default_value = "")]
    pub packet_len_plot: String,

    /// The name of the cwnd-plot-file (Leave empty to not create a plot)
    #[clap(long, short, default_value = "")]
    pub cwnd_plot: String,

    /// The name of the srtt-plot-file (Leave empty to not create a plot)
    #[clap(long, short, default_value = "")]
    pub srtt_plot: String,
}

fn add_colors() -> clap::builder::Styles {
    clap::builder::Styles::styled().literal(
        anstyle::Style::new().fg_color(Some(anstyle::Color::Ansi(anstyle::AnsiColor::Green))),
    )
}

#[rustfmt::skip]
mod unformatted {
    use std::fmt::Display;


    impl Display for super::Cli {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            // ├──
            // └──
            
            writeln!(f, "CLI Arguments").unwrap();
            writeln!(f, "  ├── load-log: {}", self.load_log).unwrap();
            writeln!(f, "  ├── load-csv: {}", self.load_csv_c).unwrap();
            writeln!(f, "  ├── save-file: {}", self.save_file).unwrap();
            writeln!(f, "  ├── csv-name: {}", self.csv_name).unwrap();
            writeln!(f, "  ├── pcap-name: {}", self.pcap_name).unwrap(); 
            writeln!(f, "  ├── ma-window: {}", self.ma_window).unwrap();
            writeln!(f, "  ├── ma-resolution: {}", self.ma_resolution).unwrap();
            writeln!(f, "  ├── adresses: {}", self.address).unwrap();
            writeln!(f, "  ├── plots: {}", self.plots).unwrap(); 
            writeln!(f, "  ├── iperf3-args: \"{}\"", self.iperf3_args).unwrap(); 
            // writeln!(f, "  ├── tshark-filter: \"{}\"", self.tshark_filter).unwrap(); 
            writeln!(f, "  ├── convert-pcap-to-csv: \"{}\"", self.convert_pcap_to_csv).unwrap(); 
            write!  (f, "  └── tcpdump-args: \"{}\"", self.tcpdump_args)
        }
    }
}

impl Display for Addresses {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if !self.src_ip.is_empty() {
            write!(f, "src_ip: {}; ", self.src_ip).unwrap();
        }
        if !self.src_port.is_empty() {
            write!(f, "src_port: {}; ", self.src_port).unwrap();
        }
        if !self.dest_ip.is_empty() {
            write!(f, "dest_ip: {}; ", self.dest_ip).unwrap();
        }
        if !self.dest_port.is_empty() {
            write!(f, "dest_port: {}; ", self.dest_port).unwrap();
        }
        Ok(())
    }
}

impl Display for Plots {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if !self.cwnd_plot.is_empty() {
            write!(f, "cwnd: {}; ", self.cwnd_plot).unwrap();
        }
        if !self.srtt_plot.is_empty() {
            write!(f, "srtt: {}; ", self.srtt_plot).unwrap();
        }
        if !self.throughput_plot.is_empty() {
            write!(f, "throughput: {}; ", self.throughput_plot).unwrap();
        }
        if !self.packet_len_plot.is_empty() {
            write!(f, "packlen: {}; ", self.packet_len_plot).unwrap();
        }

        Ok(())
    }
}
