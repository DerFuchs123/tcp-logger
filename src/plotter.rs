use std::time::Duration;

use num_format::ToFormattedString;
use plotters::{
    backend::SVGBackend,
    chart::ChartBuilder,
    coord::{combinators::IntoLinspace, ranged1d::Ranged},
    drawing::IntoDrawingArea,
    element::{Circle, EmptyElement, PathElement},
    series::{LineSeries, PointSeries},
    style::{
        full_palette::{GREY, ORANGE, PURPLE},
        Color, IntoFont, IntoTextStyle, ShapeStyle, BLACK, BLUE, RED, WHITE,
    },
};

use crate::{
    cli_args::Cli, conversions::apply_moving_average_quantized, types_client::ClientMeasurement,
    types_server::ServerMeasurement,
};

pub fn create_server_plots(cli: &Cli, server_measurements: &ServerMeasurement) {
    if !cli.plots.srtt_plot.is_empty() {
        let data_srtt = apply_moving_average_quantized(
            Duration::from_millis(10),
            Duration::from_millis(400),
            &server_measurements
                .data
                .iter()
                .filter_map(|data| {
                    let time_delta = data.time - server_measurements.start_time();
                    if time_delta.as_secs() > cli.svg_data_cutoff_time {
                        return None;
                    }
                    Some((
                        (time_delta),
                        Duration::from_micros(data.srtt).as_millis() as f64,
                    ))
                })
                .collect(),
        )
        .iter()
        .map(|(d, f)| (d.as_millis() as u64, *f))
        .collect();

        create_srtt_plot(
            &cli.plots.srtt_plot,
            "SRTT",
            "sec",
            "ms",
            (1024, 512),
            // (1536, 512),
            data_srtt,
        );
    }

    if !cli.plots.cwnd_plot.is_empty() {
        let data_cwnd = server_measurements
            .data
            .iter()
            .filter_map(|data| {
                let time_delta = data.time - server_measurements.start_time();
                if time_delta.as_secs() > cli.svg_data_cutoff_time {
                    return None;
                }
                Some(((time_delta).as_millis() as u64, data.cwnd))
            })
            .collect();

        let data_ssthresh: Vec<(u64, u64)> = server_measurements
            .data
            .iter()
            .filter_map(|data| {
                let time_delta = data.time - server_measurements.start_time();
                if time_delta.as_secs() > cli.svg_data_cutoff_time {
                    return None;
                }
                Some((time_delta.as_millis() as u64, (data.ssthresh as f64) as u64))
            })
            .collect();

        // Find the stream with the most data
        let rwnd_stream = server_measurements
            .recv_window
            .iter()
            .fold((0, 0), |(num, stream), elem| {
                if elem.2.len() > num {
                    (elem.2.len(), elem.0)
                } else {
                    (num, stream)
                }
            })
            .1;

        let bif_stream = server_measurements
            .bytes_in_flight
            .iter()
            .fold((0, 0), |(num, stream), elem| {
                if elem.2.len() > num {
                    (elem.2.len(), elem.0)
                } else {
                    (num, stream)
                }
            })
            .1;

        let (data_rwnd, data_bytes_in_flight) = if !server_measurements.recv_window.is_empty() {
            let stream = server_measurements
                .recv_window
                .iter()
                .find(|elem| elem.0 == rwnd_stream)
                .unwrap();
            let rwnd = stream
                .2
                .iter()
                .filter_map(|data| {
                    if data.0.as_secs() > cli.svg_data_cutoff_time {
                        return None;
                    }
                    Some((
                        data.0.as_millis() as u64,
                        (data.1 as f64 / stream.1 as f64).round() as u64,
                    ))
                })
                .collect();
            let bytes_in_flight_stream = server_measurements
                .bytes_in_flight
                .iter()
                .find(|elem| elem.0 == bif_stream)
                .unwrap();

            let bytes_in_flight: Vec<(Duration, u64)> = bytes_in_flight_stream
                .2
                .iter()
                .filter_map(|data| {
                    if data.0.as_secs() > cli.svg_data_cutoff_time {
                        return None;
                    }
                    Some((
                        data.0,
                        (data.1 as f64 / bytes_in_flight_stream.1 as f64).round() as u64,
                    ))
                })
                .collect();

            let mut last_time = Duration::from_millis(0);
            let mut last_max = 0;
            let bytes_in_flight = bytes_in_flight
                .iter()
                .filter_map(|data| {
                    if data.0.as_secs() > cli.svg_data_cutoff_time {
                        return None;
                    }

                    if data.1 > last_max {
                        last_max = data.1;
                    }

                    if data.0 - last_time > Duration::from_millis(25) {
                        last_time = data.0;
                        let val = last_max;
                        last_max = 0;
                        Some((data.0.as_millis() as u64, val))
                    } else {
                        None
                    }
                })
                .collect();

            (rwnd, bytes_in_flight)
        } else {
            (vec![], vec![])
        };

        // let data_snd_wnd = server_measurements
        //     .data
        //     .iter()
        //     .map(|data| {
        //         (
        //             (data.time - server_measurements.start_time()).as_millis() as u64,
        //             (data.snd_wnd as f64 / 1460.0).round() as u64,
        //         )
        //     })
        //     .collect();

        create_cwnd_recvwnd_plot(
            &cli.plots.cwnd_plot,
            "CWND, RWND & Bytes-in-Flight", // ,   & SSTHRESH
            (1024, 512),
            // (1536, 512),
            data_cwnd,
            vec![],
            // data_ssthresh,
            // vec![],
            data_rwnd,
            // vec![],
            data_bytes_in_flight,
            // data_snd_wnd,
        );
    }
}

pub fn create_client_plots(cli: &Cli, client_measurements: &ClientMeasurement) {
    if !cli.plots.throughput_plot.is_empty() {
        let stream = client_measurements.get_most_active_stream();
        let data_throughput = client_measurements
            .calculate_throughput(
                stream,
                Duration::from_millis(cli.ma_resolution),
                Duration::from_millis(cli.ma_window),
            )
            .iter()
            .filter_map(|data| {
                if data.0.as_secs() > cli.svg_data_cutoff_time {
                    return None;
                } else {
                    Some((data.0.as_millis() as u64, (data.1 / 1000.0).round() as u64))
                }
            })
            .collect();

        let data_throughput_rough = client_measurements
            .calculate_throughput(
                stream,
                Duration::from_millis(1000),
                Duration::from_millis(1000),
            )
            .iter()
            .filter_map(|data| {
                if data.0.as_secs() > cli.svg_data_cutoff_time {
                    return None;
                }
                // / 1000.0 -> Convert to Mbps
                Some((data.0.as_millis() as u64, (data.1 / 1000.0).round() as u64))
            })
            .collect();

        let data_retransmissions = client_measurements.data[stream]
            .data
            .iter()
            .filter_map(|elem| {
                if elem.time.as_secs() > cli.svg_data_cutoff_time {
                    return None;
                }

                if elem.retransmission {
                    Some((elem.time.as_millis() as u64, true))
                } else {
                    None
                }
            })
            .collect();

        let data_loss = client_measurements.data[stream]
            .data
            .iter()
            .filter_map(|elem| {
                if elem.time.as_secs() > cli.svg_data_cutoff_time {
                    return None;
                }

                if elem.loss {
                    Some((elem.time.as_millis() as u64, true))
                } else {
                    None
                }
            })
            .collect();
        create_throughput_retr_loss_plot(
            &cli.plots.throughput_plot,
            "Throughput / Loss",
            "sec",
            "Mbps",
            (1024, 512),
            // (1536, 512),
            data_throughput,
            data_throughput_rough,
            data_retransmissions,
            data_loss,
        );
    }

    if !cli.plots.packet_len_plot.is_empty() {
        let stream = client_measurements.get_most_active_stream();

        let data_pack_len = client_measurements.data[stream]
            .data
            .iter()
            .filter_map(|data| {
                if (data.time - client_measurements.start_time(stream)).as_secs()
                    > cli.svg_data_cutoff_time
                {
                    return None;
                }
                Some((
                    (data.time - client_measurements.start_time(stream)).as_millis() as u64,
                    data.ip_len as u64,
                ))
            })
            .collect();
        plotters_wrapper::create_point_plot(
            &cli.plots.packet_len_plot,
            "Packet Length (Bytes)",
            (1024, 512),
            data_pack_len,
        );
    }
}

pub fn create_throughput_retr_loss_plot(
    filename: impl Into<String>,
    title: impl Into<String>,
    x_desc: impl Into<String>,
    y_desc: impl Into<String>,
    img_size: (u32, u32),
    thr: Vec<(u64, u64)>,
    thr_rough: Vec<(u64, u64)>,
    retr: Vec<(u64, bool)>,
    loss: Vec<(u64, bool)>,
) {
    let filename = filename.into();
    let title = title.into();

    let root_area = SVGBackend::new(&filename, img_size).into_drawing_area();

    let start_x = thr.first().unwrap().0;
    let end_x = thr.last().unwrap().0;

    let min_y = thr
        .iter()
        .min_by(|elem1, elem2| elem1.1.cmp(&elem2.1))
        .unwrap()
        .1;

    let max_y = thr
        .iter()
        .max_by(|elem1, elem2| elem1.1.cmp(&elem2.1))
        .unwrap()
        .1;

    root_area.fill(&WHITE).unwrap();

    let x_axis = (start_x..end_x).step(1);

    let mut cc = ChartBuilder::on(&root_area)
        .margin(5)
        .set_all_label_area_size(50)
        .caption(title, ("sans-serif", 30))
        .build_cartesian_2d(x_axis.range(), min_y..max_y)
        .unwrap();

    cc.configure_mesh()
        .x_labels(12)
        .y_labels(10)
        .max_light_lines(10)
        .x_label_formatter(&|v| Duration::from_millis(*v).as_secs().to_string())
        .y_label_formatter(&|v| v.to_formatted_string(&num_format::Locale::de).to_string())
        .x_label_style(("sans-serif", 20).into_font())
        .y_label_style(("sans-serif", 16).into_font())
        .y_desc(y_desc)
        .x_desc(x_desc)
        .draw()
        .unwrap();

    cc.draw_series(LineSeries::new(
        thr.iter().map(|data| (data.0, data.1)),
        &GREY,
    ))
    .unwrap()
    .label("Throughput")
    .legend(move |(x, y)| {
        PathElement::new([(x, y), (x + 15, y)], ShapeStyle::from(&GREY).filled())
    });
    cc.draw_series(LineSeries::new(
        thr_rough.iter().map(|data| (data.0, data.1)),
        &BLUE,
    ))
    .unwrap()
    .label("Throughput (rough)")
    .legend(move |(x, y)| {
        PathElement::new([(x, y), (x + 15, y)], ShapeStyle::from(&BLUE).filled())
    });

    cc.draw_series(PointSeries::of_element(
        loss.iter().map(|e| (e.0, e.1 as u64)),
        3,
        ShapeStyle::from(&RED).filled(),
        &|coord, size, style| EmptyElement::at(coord) + Circle::new((0, 0), size, style),
    ))
    .unwrap()
    .label("Segment Loss")
    .legend(move |(x, y)| Circle::new((x + 7, y), 3, ShapeStyle::from(&RED).filled()));

    cc.draw_series(PointSeries::of_element(
        retr.iter().map(|e| (e.0, e.1 as u64)),
        3,
        ShapeStyle::from(&PURPLE).filled(),
        &|coord, size, style| EmptyElement::at(coord) + Circle::new((0, 0), size, style),
    ))
    .unwrap()
    .label("Retransmission")
    .legend(move |(x, y)| Circle::new((x + 7, y), 3, ShapeStyle::from(&PURPLE).filled()));

    cc.configure_series_labels()
        .label_font(("sans-serif", 20))
        .border_style(BLACK)
        .background_style(WHITE.mix(0.6))
        .position(plotters::chart::SeriesLabelPosition::UpperRight)
        .draw()
        .unwrap();

    root_area.present().unwrap();
}

pub fn create_srtt_plot(
    filename: impl Into<String>,
    title: impl Into<String>,
    x_desc: impl Into<String>,
    y_desc: impl Into<String>,
    img_size: (u32, u32),
    values: Vec<(u64, f64)>,
) {
    let filename = filename.into();
    let title = title.into();

    let root_area = SVGBackend::new(&filename, img_size).into_drawing_area();

    let start_x = values.first().unwrap().0;
    let end_x = values.last().unwrap().0;

    let min_y: f64 = 0.0;
    // values
    //     .iter()
    //     .min_by(|elem1, elem2| elem1.1.total_cmp(&elem2.1))
    //     .unwrap()
    //     .1;

    let max_y = 1000.0_f64.max(
        values
            .iter()
            .max_by(|elem1, elem2| elem1.1.total_cmp(&elem2.1))
            .unwrap()
            .1,
    );

    root_area.fill(&WHITE).unwrap();

    let x_axis = (start_x..end_x).step(1);

    let mut cc = ChartBuilder::on(&root_area)
        .margin(5)
        .set_all_label_area_size(50)
        .caption(title, ("sans-serif", 30))
        .build_cartesian_2d(x_axis.range(), min_y..max_y)
        .unwrap();

    cc.configure_mesh()
        .x_labels(12)
        .y_labels(10)
        .max_light_lines(10)
        .x_label_formatter(&|v| Duration::from_millis(*v).as_secs().to_string())
        .y_label_formatter(&|v| v.to_string())
        .label_style(("sans-serif", 18).into_font())
        .x_label_style(("sans-serif", 20).into_font())
        .y_label_style(("sans-serif", 20).into_font())
        .y_desc(y_desc)
        .x_desc(x_desc)
        .draw()
        .unwrap();

    cc.draw_series(LineSeries::new(
        values.iter().map(|data| (data.0, data.1)),
        &RED,
    ))
    .unwrap();

    root_area.present().unwrap();
}

pub fn create_cwnd_recvwnd_plot(
    filename: impl Into<String>,
    title: impl Into<String>,
    img_size: (u32, u32),
    values: Vec<(u64, u64)>,
    values_ssthresh: Vec<(u64, u64)>,
    values_rwnd: Vec<(u64, u64)>,
    values_bytes_in_flight: Vec<(u64, u64)>,
    // values_snd_wnd: Vec<(u64, u64)>,
) {
    let filename = filename.into();
    let title = title.into();

    let root_area = SVGBackend::new(&filename, img_size).into_drawing_area();

    let start_x = values.first().unwrap().0;
    let end_x = values.last().unwrap().0;

    let min_y = values
        .iter()
        // .chain(values_ssthresh.iter())
        .chain(values_rwnd.iter())
        .chain(values_bytes_in_flight.iter())
        .min_by(|elem1, elem2| elem1.1.cmp(&elem2.1))
        .unwrap()
        .1;

    let max_y = values
        .iter()
        // .chain(values_ssthresh.iter())
        .chain(values_rwnd.iter())
        .chain(values_bytes_in_flight.iter())
        .max_by(|elem1, elem2| elem1.1.cmp(&elem2.1))
        .unwrap()
        .1;

    root_area.fill(&WHITE).unwrap();

    let x_axis = (start_x..end_x).step(1);

    let mut cc = ChartBuilder::on(&root_area)
        .margin(5)
        .set_all_label_area_size(50)
        .caption(title, ("sans-serif", 30))
        .build_cartesian_2d(x_axis.range(), min_y..max_y)
        .unwrap();
    // .set_secondary_coord(x_axis.range(), 0..1000000 as u64);

    cc.configure_mesh()
        .x_labels(12)
        .y_labels(10)
        .max_light_lines(10)
        .x_label_formatter(&|v| Duration::from_millis(*v).as_secs().to_string())
        .y_label_formatter(&|v| v.to_string())
        .label_style(("sans-serif", 18).into_font())
        .x_label_style(("sans-serif", 20).into_font())
        .y_label_style(("sans-serif", 20).into_font())
        .y_desc("MSS")
        .x_desc("secs")
        .draw()
        .unwrap();

    // cc.configure_secondary_axes()
    //     .y_desc("recv-window-als")
    //     .label_style("sans-serif".with_color(PURPLE))
    //     .y_label_formatter(&|v| v.to_string())
    //     .x_label_formatter(&|v| Duration::from_millis(*v).as_secs().to_string())
    //     .draw()
    //     .unwrap();

    if !values.is_empty() {
        cc.draw_series(LineSeries::new(values, &RED))
            .unwrap()
            .label("cwnd")
            .legend(move |(x, y)| Circle::new((x + 7, y), 3, ShapeStyle::from(&RED).filled()));
    }

    if !values_ssthresh.is_empty() {
        cc.draw_series(LineSeries::new(values_ssthresh, &ORANGE))
            .unwrap()
            .label("ssthresh")
            .legend(move |(x, y)| Circle::new((x + 7, y), 3, ShapeStyle::from(&ORANGE).filled()));
    }

    if !values_rwnd.is_empty() {
        cc.draw_series(LineSeries::new(values_rwnd, &BLUE))
            .unwrap()
            .label("rwnd")
            .legend(move |(x, y)| Circle::new((x + 7, y), 3, ShapeStyle::from(&BLUE).filled()));
    }

    // cc.draw_series(LineSeries::new(values_bytes_in_flight, &PURPLE))
    //     .unwrap()
    //     .label("bytes-in-flight")
    //     .legend(move |(x, y)| Circle::new((x + 7, y), 3, ShapeStyle::from(&PURPLE).filled()));

    if !values_bytes_in_flight.is_empty() {
        cc.draw_series(PointSeries::of_element(
            values_bytes_in_flight,
            1,
            ShapeStyle::from(&PURPLE).filled(),
            &|coord, size, style| EmptyElement::at(coord) + Circle::new((0, 0), size, style),
        ))
        .unwrap()
        .label("max bytes-in-flight")
        .legend(move |(x, y)| Circle::new((x + 7, y), 3, ShapeStyle::from(&PURPLE).filled()));
    }

    // cc.draw_series(LineSeries::new(values_snd_wnd, &ORANGE))
    //     .unwrap()
    //     .label("snd-wnd")
    //     .legend(move |(x, y)| Circle::new((x + 7, y), 3, ShapeStyle::from(&ORANGE).filled()));

    cc.configure_series_labels()
        .label_font(("sans-serif", 20))
        .border_style(BLACK)
        .background_style(WHITE.mix(0.6))
        .position(plotters::chart::SeriesLabelPosition::UpperLeft)
        .draw()
        .unwrap();

    root_area.present().unwrap();
}
