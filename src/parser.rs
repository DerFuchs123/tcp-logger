use std::collections::HashMap;

pub fn parse_line(line: &str) -> Option<HashMap<String, String>> {
    let split: Vec<&str> = line
        .split(' ')
        .filter_map(|line| {
            if line.is_empty() {
                None
            } else {
                Some(line.trim())
            }
        })
        .collect();

    let ind = split.iter().position(|line| line.contains("tcp_probe:"));

    if ind.is_none() {
        eprintln!("No line 'tcp_probe:' found in '{:?}'", line);
        return None;
    }

    let ind = ind.unwrap();

    let mut data = HashMap::new();
    data.insert(
        "time".to_string(),
        split[ind - 1].trim_end_matches(':').to_string(),
    );

    let split = split[ind + 1..].to_vec();

    for elem in split {
        let split: Vec<String> = elem.split('=').map(str::to_string).collect();
        data.insert(split[0].clone(), split[1].clone());
    }

    Some(data)
}
