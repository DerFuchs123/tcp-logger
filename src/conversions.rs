use std::{process::Stdio, time::Duration};

pub fn apply_moving_average_timed(
    resolution: Duration,
    window: Duration,
    data: &Vec<(Duration, f64)>,
) -> Vec<(Duration, f64)> {
    let mut throughput = moving_average_group_step(resolution, window, data);

    for (_, (_, f)) in &mut throughput {
        *f /= window.as_secs_f64();
    }

    throughput
        .iter()
        .map(|elem| (elem.1 .0, elem.1 .1))
        .collect()
}

pub fn apply_moving_average_quantized(
    resolution: Duration,
    window: Duration,
    data: &Vec<(Duration, f64)>,
) -> Vec<(Duration, f64)> {
    let mut throughput = moving_average_group_step(resolution, window, data);

    for (c, (_, f)) in &mut throughput {
        *f /= *c as f64;
    }

    throughput
        .iter()
        .map(|elem| (elem.1 .0, elem.1 .1))
        .collect()
}

/// Applies a general moving average filter for the provided elements.
/// This Function **can not be used by itself!**
/// The Returned values have to be either divided by the quantity or the time
/// from which they were provided. See ` apply_moving_average_quantized ` and
/// ` apply_moving_average_timed `.
pub fn moving_average_group_step(
    resolution: Duration,
    window: Duration,
    data: &Vec<(Duration, f64)>,
) -> Vec<(u32, (Duration, f64))> {
    let mut throughput: Vec<(u32, (Duration, f64))> = vec![(0, (Duration::from_millis(0), 0.0))];
    for data in data {
        let last_elem = throughput.last().unwrap();
        if data.0 - last_elem.1 .0 > resolution {
            throughput.push((0, (data.0, 0.0)))
        }
    }

    for (ctr, (curr_time, thr)) in &mut throughput {
        let start_pos = data
            .iter()
            .position(|elem| {
                elem.0.as_secs_f64() > curr_time.as_secs_f64() - window.as_secs_f64() / 2.0
            })
            .unwrap_or(0);

        let end_pos = data
            .iter()
            // .skip(start_pos)
            .position(|elem| {
                elem.0.as_secs_f64() > curr_time.as_secs_f64() + window.as_secs_f64() / 2.0
            })
            .unwrap_or(data.len());
        for elem in &data[start_pos..end_pos] {
            *thr += elem.1;
        }
        *ctr = data[start_pos..end_pos].len() as u32;
    }

    throughput
}

pub fn client_pcap_to_csv(input_file: impl Into<String>, output_file: impl Into<String>) {
    println!("<Client Mode>");

    let input_file = input_file.into();
    let output_file = output_file.into();

    let program = "tshark";

    let flags = vec![
        "-r",
        &input_file,
        "-T",
        "fields",
        "-E",
        "separator=|",
        "-e",
        "tcp.stream",
        "-e",
        "frame.time_epoch",
        "-e",
        "ip.addr",
        "-e",
        "tcp.port",
        "-e",
        "ip.len",
        "-e",
        "ip.hdr_len",
        "-e",
        "tcp.hdr_len",
        "-e",
        "tcp.options.mss_val",
        "-e",
        "tcp.analysis.lost_segment",
        "-e",
        "tcp.analysis.retransmission",
        "-e",
        "tcp.seq_raw",
        "-R",
        "tcp",
        "-2",
        "-Y",
        "tcp.srcport==5201",
    ];

    let file = std::fs::File::create(output_file).unwrap();
    let stdout = Stdio::from(file);

    std::process::Command::new(program)
        .args(flags)
        .stdout(stdout)
        .spawn()
        .unwrap()
        .wait()
        .unwrap();
}

pub fn server_pcap_to_csv(
    input_file: impl Into<String>,
    output_file_dstport: impl Into<String>,
    output_file_srcport: impl Into<String>,
) {
    println!("<Server Mode>");

    let input_file = input_file.into();
    let output_file_d = output_file_dstport.into();
    let output_file_s = output_file_srcport.into();

    let program = "tshark";

    println!("Starting Conversions 1 & 2...");
    /*
        To convert the recv-window from bytes to mss,
        we need to divide it by the mss, which was sent to us by the client.
        This value can also be obtained by setting the dstport==5201
    */
    let flags = vec![
        "-r",
        &input_file,
        "-T",
        "fields",
        "-E",
        "separator=|",
        "-e",
        "tcp.stream",
        "-e",
        "frame.time_epoch",
        "-e",
        "tcp.window_size",
        "-e",
        "tcp.options.mss_val",
        "-R",
        "tcp",
        "-2",
        "-Y",
        "tcp.dstport==5201",
    ];

    let file = std::fs::File::create(output_file_d).unwrap();
    let stdout = Stdio::from(file);

    let mut conv1 = std::process::Command::new(program)
        .args(flags)
        .stdout(stdout)
        .spawn()
        .unwrap();

    let flags = vec![
        "-r",
        &input_file,
        "-T",
        "fields",
        "-E",
        "separator=|",
        "-e",
        "tcp.stream",
        "-e",
        "frame.time_epoch",
        "-e",
        "tcp.analysis.bytes_in_flight",
        "-e",
        "tcp.options.mss_val",
        "-e",
        "tcp.seq_raw",
        "-R",
        "tcp",
        "-2",
        "-Y",
        "tcp.srcport==5201",
    ];

    let file = std::fs::File::create(output_file_s).unwrap();
    let stdout = Stdio::from(file);

    let mut conv2 = std::process::Command::new(program)
        .args(flags)
        .stdout(stdout)
        .spawn()
        .unwrap();

    conv1.wait().unwrap();
    println!("Conversion 1 Done!");

    conv2.wait().unwrap();
    println!("Conversion 2 Done!");
}
