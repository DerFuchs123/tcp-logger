#!/bin/bash

./clean.sh

cargo build --release

MES_TIME=200; 

# switch statement for the first argument
case $1 in
    # run server
    # capture outgoing acks to measure cwnd and srtt? 
    "s_pw")
        sudo ./target/release/tcp-logger --src-port="5201" -i="-s" -d="-i eth0 -U -s96 -n src port 5201 or dst port 5201" -p="tcpdump_server.pcap" --save-file="server_data.log" -c="cwnd_ssthresh_plot.svg" -s="srtt.svg" -o="mes" &
        ;;

    "s_ikt_s1" | "s")
        sudo ./target/release/tcp-logger --src-port="5201" -i="-s" -d="-i eno4 -U -s96 -n src port 5201 or dst port 5201" -p="tcpdump_server.pcap" --save-file="server_data.log" -c="cwnd_ssthresh_plot.svg" -s="srtt.svg" -o="mes" &
        ;; 

    # run the client with pastawraps as the server
    "c_pw_r")
        sudo ./target/release/tcp-logger -i="-c 193.31.24.188 -t $MES_TIME -R" -d="-i wlp0s20f3 -U -s96 -n src port 5201 or dst port 5201" -p="tcpdump_client.pcap" --dest-ip="193.31.24.188" --dest-port="5201" --save-file="client_data.log" -o="mes" -h &
        ;;

    # run the client with ikt-s1 as the server
    "c_ikt_s1_r" | "c")
        sudo ./target/release/tcp-logger -i="-c 130.75.73.69 -t $MES_TIME -R -C cubic" -d="-i wwan0 -U -s96 -n src port 5201 or dst port 5201" -p="tcpdump_client.pcap" --dest-ip="130.75.73.69" --dest-port="5201" --save-file="client_data.log" -o="mes" -b -h --serial-reader-args="-d" & 
        ;;

    "c_ikt_s1_r_l" | "c_l")
        sudo ./target/release/tcp-logger -i="-c 130.75.73.69 -t $MES_TIME -R -C cubic" -d="-i wwan0 -U -s96 -n src port 5201 or dst port 5201" -p="tcpdump_client.pcap" --dest-ip="130.75.73.69" --dest-port="5201" --save-file="client_data.log" -o="mes" -b -h --serial-reader-args="-d" & 
        ;;

esac

MES_PID=$!; 

if [[ $1 == "c" ]]; then
    MES_TIME=$(($MES_TIME + 1)); 
    echo "Sleeping for $MES_TIME seconds";
    sleep $MES_TIME; 
    sudo kill -SIGINT $MES_PID; 
fi


wait $MES_PID; 